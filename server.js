const express = require('express')
const bookRouter= express.Router() 
const app = express()
const bodyParser= require('body-parser')


app.use(bodyParser.json()) 
app.use(bodyParser.urlencoded({ extended: true }))


var MongoClient = require('mongodb').MongoClient;
var ObjectID = require('mongodb').ObjectID;
MongoClient.connect('mongodb://flokflokk:flok3399@ds030827.mlab.com:30827/ead_final', (err, db) => {
  if (err) return console.log(err)

  app.listen(3000, () => {
    console.log('app working on 3000')
  });

  let dbase = db.db("ead_final");

  app.post('/books', (req, res, next) => {

    let books = {
      id: req.body.id,
      first_n: req.body.first_n,
      last_n: req.body.last_n,
    };//สร้าง

    dbase.collection("books").save(books, (err, result) => {
      if (err) {
        console.log(err);
      }

      res.send('books added successfully');
    });

  });

  app.get('/books', (req, res, next) => {
    dbase.collection('books').find().toArray((err, results) => {
      res.send(results)
    });//เรียก
  });

  app.get('/books/:id', (req, res, next) => {
    if (err) {
      throw err;
    }//เรียกตามid

    let id = ObjectID(req.params.id);
    dbase.collection('books').find(id).toArray((err, result) => {
      if (err) {
        throw err;
      }

      res.send(result);
    });
  });

  app.put('/books/:id', (req, res, next) => {
    var id = {
      _id: new ObjectID(req.params.id)
    };//update

    dbase.collection("books").update(id, { $set: { id: req.body.id,
        first_n: req.body.first_n,
        last_n: req.body.last_n, } }, (err, result) => {
      if (err) {
        throw err;
      }

      res.send('user updated sucessfully');
    });
  });


  app.delete('/books/:id', (req, res, next) => {
    let id = ObjectID(req.params.id);
        //delete
    dbase.collection('books').deleteOne({ _id: id }, (err, result) => {
      if (err) {
        throw err;
      }

      res.send('book deleted');
    });
  });
});    